﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundManager : MonoBehaviour
{
    private float textureOffset = 0.0f;
    private Material mat;
    private Vector2 scroll;
    //Smooth ha de ser un valor menor que cero!!
    public float smooth;

    private float axis;

    void Awake(){
        mat = GetComponent<Renderer> ().material;
        scroll = new Vector2 (textureOffset, 0);
    }

    void Update(){
        textureOffset += (Time.deltaTime * ((75*axis/100) * smooth));
        scroll.x = textureOffset;
        mat.SetTextureOffset("_MainTex", scroll);
    }
    
    public void SetVelocity(float velx){
        axis = velx;
    }
}
