﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float SpeedMax;
    private Rigidbody2D rb;
    public float FuerzaDeSalto;
    private bool jump;
    private bool grounded;
    private bool attack;
    private bool jump_throw;
   
     [SerializeField] float velocitymax;
    //Variables para el movimiento
    [SerializeField] int movementSpeed;
    private float h;
    private Animator anim;
<<<<<<< HEAD
    private Collider2D coll;
=======
    //Variable para el movimiento del BG
    public BackgroundManager bg;
>>>>>>> 1038989012e248f36c9a6aa67dcce6776ba27718

    void Awake(){
        //LLamo al componente rigidbody
        rb = GetComponent<Rigidbody2D>();
        gameObject.AddComponent<BoxCollider2D>();
        anim = GetComponent<Animator>();
        coll  = GetComponentInChildren<Collider2D>();
        coll.enabled=false;
    }
        void FixedUpdate(){
        Movimiento();
        Animations();
        //Para que haya friccion
        Vector3 fixedVelocity = rb.velocity;
        fixedVelocity.x *= 0.75f;
        if(grounded){
            rb.velocity = fixedVelocity;
        }
        //Invertir Escala cuando va en negativo
        if(h > 0.1f){
            transform.localScale= new Vector3(0.52906f,0.52906f,1f);
        }else if(h < -0.1f){
            transform.localScale= new Vector3(-0.52906f,0.52906f,1f);
        }

        //Limite Izquierda
        if(transform.position.x <= -80f){
            transform.position = new Vector3(-80f, transform.position.y, transform.position.z);
        }
    }

    void Update(){
       //Comprobar si esta en el suelo para saltar
        if (Input.GetButtonDown("Jump") && grounded){
           jump = true;
       }
       bg.SetVelocity(h);
    }


    public void Movimiento(){
        //Movimiento player
        h = Input.GetAxis("Horizontal");
         rb.AddForce(Vector2.right*h*movementSpeed*2);
         //Limitar velocidad al player
          SpeedMax = Mathf.Clamp(rb.velocity.x,-velocitymax,velocitymax);
         rb.velocity = new Vector2(SpeedMax,rb.velocity.y);
             
        
        //Salto
         //Si Jump es true salta
         if(jump){
           rb.AddForce(Vector2.up*FuerzaDeSalto,ForceMode2D.Impulse);
           jump = false;
       }
    }

    public void OnCollisionStay2D(Collision2D other) {
        if(other.gameObject.tag == "Suelo"){
            grounded = true;     
        }
    }
     public void OnCollisionExit2D(Collision2D other) {
        if(other.gameObject.tag == "Suelo"){
            grounded = false;
        }
    } 

    //ANIMACIONES
    public void Animations(){
        anim.SetBool("Grounded", grounded);
       //Hacer animacion de atacar
       if (Input.GetKeyDown(KeyCode.C) && grounded){
           //Ejecutar animacion de atacar
            anim.SetTrigger("Attack");
            Debug.Log("Ataque");
        }
        else if(Input.GetKeyDown(KeyCode.C) && !grounded){
            //Ejecutar animacion de atacar mientras estas saltando
            anim.SetTrigger("jump_attack");
            Debug.Log("Ataque en el aire");
        }
        //Tirar Kunai 
       if (Input.GetKeyDown(KeyCode.X) && grounded){
           //Ejecutar animacion de atacar
           anim.SetTrigger("attack_throw");
           Debug.Log("Ataque Kunai");
       }
        //Ejecutar animacion de atacar mientras estas saltando
        else if(Input.GetKeyDown(KeyCode.X) && !grounded){
           anim.SetTrigger("jump_throw");
           Debug.Log("Ataque Kunai en el aire");
           }
       //Si la velocidad es mayor que 0.1f(Valor indicado en el animation) ejecuta la animacion de
       anim.SetFloat("Speed", Mathf.Abs(rb.velocity.x));
    }
    
   

    
}
