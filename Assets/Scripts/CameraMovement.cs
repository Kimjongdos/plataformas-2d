﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Transform target;

    void Update(){
        //La camara sigue la posicion X del Player
        this.transform.position = new Vector3(target.position.x, transform.position.y, this.transform.position.z);
    }
}
